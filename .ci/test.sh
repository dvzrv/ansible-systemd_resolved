#!/bin/bash
#
# SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
# SPDX-License-Identifier: GPL-3.0-or-later

set -euxo pipefail

molecule test --all
