<!--
SPDX-FileCopyrightText: 2023 David Runge <dvzrv@archlinux.org>
SPDX-License-Identifier: CC-BY-SA-4.0
-->

# archlinux.systemd_resolved

An [Ansible](https://www.ansible.com/) role to configure
[systemd-resolved](https://www.freedesktop.org/software/systemd/man/systemd-resolved.service.html)
via a configuration drop-in (see [man
resolved.conf](https://www.freedesktop.org/software/systemd/man/resolved.conf.html)
for further information) to setup [Domain Name System
 (DNS)](https://en.wikipedia.org/wiki/Domain_Name_System) resolving on a Linux
host.

## Requirements

An Arch Linux system or an Arch Linux chroot.

## Role Variables

* `systemd_resolved`: a dict providing the configuration for systemd-resolved (defaults to unset)
  * `dns`: a list of strings representing DNS servers (defaults to `[]`)
  * `fallbackdns`: a list of strings representing fallback DNS servers (defaults to `['1.1.1.1#cloudflare-dns.com', '9.9.9.9#dns.quad9.net', '8.8.8.8#dns.google', '2606:4700:4700::1111#cloudflare-dns.com', '2620:fe::9#dns.quad9.net', '2001:4860:4860::8888#dns.google']`)
  * `domains`: a list of strings representing search domains (defaults to `[]`)
  * `dnssec`: a boolean value or the string `allow-downgrade` indicating how to handle DNSSEC (defaults to `false`)
  * `dnsovertls`: a boolean value or the string `opportunistic`, indicating how to handle DNSoverTLS (defaults to `false`)
  * `multicastdns`: a boolean value or the string `resolve`, indicating how to handle Multicast DNS (defaults to `true`)
  * `llmnr`: a boolean value or the string `resolve`, indicating how to handle Link-Local Multicast Name Resolution (LLMNR) (defaults to `true`)
  * `cache`: a boolean value or the string `opportunistic`, indicating how to handle cache (defaults to `true`)
  * `cachefromlocalhost`: a boolean value indicating whether to cache host-local DNS requests (defaults to `false`)
  * `dnsstublistener`: a boolean value or the strings `tcp` or `udp`, indicating how to handle DNS stub listener (defaults to `true`)
  * `dnsstublistenerextra`: a list of strings representing IPv4 or IPv6 addresses to listen to (defaults to `[]`)
  * `readetchosts`: a boolean value indicating whether to read `/etc/hosts` (defaults to `true`)
  * `resolveunicastsinglelabel`: a boolean value indicating whether to resolve `A` and `AAAA` queries for single-label names over classic DNS (defaults to `false`)
  * `stub_symlink`: a boolean value indicating whether to create a symlink from `/run/systemd/resolve/stub-resolv.conf` to `/etc/resolv.conf` (defaults to `true`)
  * `service`: a boolean value indicating whether to enable (and start) `systemd-resolved.service` (defaults to `true`)

The following variables have no defaults in this role, but are available to override behavior and use special functionality:

* `chroot_dir`: the directory to chroot into, when including `chroot.yml` tasks (defaults to unset)

## Dependencies

None

## Example Playbook

```yaml
- name: Set a custom DNS server
  hosts: my_hosts
  vars:
    systemd_resolved:
      dns:
        - 8.8.8.8
  tasks:
    - name: Include archlinux.systemd_resolved
      ansible.builtin.include_role:
        name: archlinux.systemd_resolved
```

## License

GPL-3.0-or-later

## Author Information

David Runge <dvzrv@archlinux.org>
